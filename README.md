# Estructuras de Datos - CIU - 2020

Clases de los Miércoles.
Docente: Román García

## Sobre las clases

EEDD es una materia eminentemente práctica. Cada clase se guardará como un archivo Haskell donde el nombre corresponde a la fecha en que se dió en el formato AAAAMMDD.

## El repositorio

Las clases se guardan en el repositorio público `https://gitlab.com/2da-camada/eedd-clases-roman.git` en gitLab.

## Sobre los TADs

Los tipos algebraicos que se usan para representar los valores de los tipos abstractos en general no deberían incluir `deriving Show` y menos aún `deriving Eq`, porque exponen comportamiento de la implementación y agregan "suprepticiamente" a las operaciones `show` y `(==)` al TAD.



A pesar de ello, he agregado a cada TAD un pretty-printing para poder mostrar mejor los valores abstractos que representan, sólo con fines didácticos.


- Conjuntos: el conjunto construido con `addS 4 (addS 15 emptyS)` se muestra como `{4,15}`.
- Colas: la cola construida con `queue 3 (queue 2 (queue 1 emptyQ))` se muestra como `<1,2,3<`   (los signos < ayudan a ver por donde entran y salen los elementos).
- Pilas: la pila construida con `push 8 (push 2 (push 1 emptyST))` se muestra como `~8,2,1]`   (los elementos entran y salen del mismo extremo).
- Maps: el diccionario construido con `assocM 1 "uno" (assocM 3 "tres" emptyM)` se muestra como `{3->"tres", 1->"uno"}` (se muestra como un conjunto de asociaciones).
- MultiSet: el multiconjunto construido con `addMS 3 (addMS 8 (addMS 7 (addMS 3 emptyMS)))` se muestra como `{{3,3,7,8}}` (dobles llaves para no confundirlo con un set).


