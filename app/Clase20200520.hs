module Clase20200520 where
import TAD_Queue_Listas
import TAD_Stack_Listas
{- Hoy vamos a terminar el TAD Queue, haciendo bien clara la distinción entre
   lo que va dentro del TAD y lo que no
-}

------------------------------------ COLAS ------------------------------
-- Definimos algunos valores del tipo Queue

cola1 = queue 3 (queue 2 (queue 1 emptyQ))
cola2 = queue 5 (queue 3 (queue 5 (queue 6 emptyQ))) 


---------- FUNCIONES COMO USUARIOS DEL TAD QUEUE -----------

--Cuenta la cantidad de elementos de la cola.
lengthQ :: Queue a -> Int
lengthQ q = if isEmptyQ q 
            then 0 
            else 1 + (lengthQ (dequeue q))




------------------------------------ PILAS ------------------------------

stack1 = push 8 (push 2 (push 1 emptyST))  
stack2 = push 3 emptyST


-- Cuenta la cantidad de elementos de la pila
lengthST :: Stack a -> Int
lengthST s = if isEmptyST s 
            then 0 
            else 1 + (lengthST (pop s))



-- Dada una lista devuelve una pila sin alterar el orden de los elementos
-- Si hacemos la recursión "normal", nos quedan los elementos puestos al reves en la pila
-- apilar' [1,2]  es    push 1 (push 2 empty), o sea, el ultimo elemento se pushea primero
apilar :: [a] -> Stack a
apilar xs = apilar' (reverse xs)

apilar':: [a] -> Stack a
apilar'   []   = emptyST
apilar' (x:xs) = push x (apilar' xs)

-- Si pasamos el stack al cual queremos agregarle items, se puede hacer esta otra recursión:
apilar2 :: [a] -> Stack a
apilar2 xs = apilar2' xs emptyST

apilar2' []     s = s
apilar2' (x:xs) s = apilar2' xs (push x s)

