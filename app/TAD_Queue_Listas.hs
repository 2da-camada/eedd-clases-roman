module TAD_Queue_Listas (Queue, emptyQ, isEmptyQ, queue, firstQ, dequeue) where

-- Defino el tipo algebraico que voy a usar para representar la cola
-- INVARIANTE DE REPRESENTACION: Los elementos deben encolarse por el final de la lista y desencolarse por delante.
data Queue a = Queue [a]




-- Crea una cola vacía
emptyQ :: Queue a
emptyQ = Queue [] 


-- Dada una cola indica si la cola está vacía
isEmptyQ :: Queue a -> Bool
isEmptyQ (Queue [])  = True
isEmptyQ      _      = False


-- Dados un elemento y una cola, agrega ese elemento a la cola (al final de la lista)
queue :: a -> Queue a -> Queue a
queue e (Queue xs) = Queue (xs ++ [e])


-- Dada una cola devuelve el primer elemento de la cola
-- NOTA: Falla cuando la cola está vacía
firstQ :: Queue a -> a
firstQ (Queue []) = error "cola vacía"
firstQ (Queue (x:xs)) = x 


-- Dada una cola la devuelve sin su primer elemento
-- NOTA: Falla cuando la cola está vacía
dequeue :: Queue a -> Queue a
dequeue (Queue []) = error "cola vacía"
dequeue (Queue (x:xs)) = Queue xs




















---------- No dar importancia, es para que se muestren más lindo ------
instance Show a => Show (Queue a) where
    show (Queue xs) = "<"++ mostrarItems xs ++ "<"

mostrarItems :: Show a => [a] -> String
mostrarItems    []    = ""
mostrarItems    [x]   = show x
mostrarItems (x:y:ys) = show x ++ "," ++ mostrarItems (y:ys)
