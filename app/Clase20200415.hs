module Clase20200415 where

{- Juego de Piedra, Papel y Tijera:
 a) Definir un tipo para las jugadas
 b) Definir un tipo partida. Una partida tiene el nombre de los jugadores y sus listas de jugadas
 c) Dada una partida, determinar el nombre del jugador ganador. Si ninguno gana, devolver "EMPATADOS"
 d) Dada una lista de jugadas, determinar si todas son iguales
 e) Dada una lista de jugadas, devolver una 3-tupla con las cantidades de veces que aparece cada jugada
 -}


-- Ejercicios a) y b)

data Jugada = Piedra | Papel | Tijera deriving (Eq, Show)

type Nombre = String
data Partida = Partida Nombre [Jugada] Nombre [Jugada] deriving (Show)

-- Algunos valores de tipo Partida
partida = Partida "Juan" [Piedra, Tijera, Tijera] "Pedro" [Piedra, Papel, Tijera] 


-- Ejercicio c)

ganador :: Partida -> Nombre
ganador (Partida nom1 jugadas1 nom2 jugadas2) = if jugadas1 `leGananA` jugadas2
                                                   then nom1
                                                   else if jugadas2 `leGananA` jugadas1
                                                           then nom2
                                                           else "EMPATADOS"   


-- Funciones auxiliares
leGananA :: [Jugada] -> [Jugada] -> Bool
leGananA []     []       = False
leGananA (j:js) (j':js') = j `leGanaA` j' || leGananA js js'


leGanaA :: Jugada -> Jugada -> Bool
leGanaA Piedra Tijera = True
leGanaA Papel  Piedra = True
leGanaA Tijera Papel  = True
leGanaA   _      _    = False


-- Ejercicio d)

-- Ejemplo: sonTodasLasJugadasIguales [Piedra,Piedra,Tijera] -> False
-- Ejemplo: sonTodasLasJugadasIguales [Piedra,Piedra]        -> True
sonTodasLasJugadasIguales :: [Jugada] -> Bool
sonTodasLasJugadasIguales []       = True
sonTodasLasJugadasIguales [_]      = True
sonTodasLasJugadasIguales (x:y:ys) = (x == y) && sonTodasLasJugadasIguales (y:ys)


-- Ejercicio e)

aparicionesJugadas :: [Jugada] -> (Int, Int, Int)
aparicionesJugadas js = (apariciones Piedra js, apariciones Papel js, apariciones Tijera js)

-- Esta es la misma función que se hizo en la Practica 1
apariciones :: Eq a => a -> [a] -> Int
apariciones a [] = 0
apariciones a (x:xs) | x == a    = 1 + apariciones a xs
                     | otherwise =     apariciones a xs
