module Clase20200513 where
import TAD_Set_ListaSinRepetidos

{- En la clase de hoy creamos nuestro primer Tipo de Datos Abstracto (TAD). La implementación
   que vamos a hacer del conjunto no es la que pide la práctica, sino una más sencilla que pueden
   usar de ejemplo.
   Es importante ponerse del lado adecuado de la cancha:
     - O sos usuario del TAD (y por lo tanto sólo se pueden usar la funciones definidas, normalmente
       llamado contrato, especificación o interface. Por ejemplo, NO SE PUEDE usar pattern matching)
     - O sos implementador del TAD (y por lo tanto tenés acceso a cómo esta hecho. Por ejemplo, se
       puede hacer pattern matching)  
-}
-- En este archivo somos USUARIOS del tipo abstracto

-- Defino algunos valores del tipo abstracto conjunto
set0  = emptyS                      -- {}
set1  = 'a' `addS` emptyS           -- {'a'}
set1' = addS 'a' emptyS             -- {'a'}
set2  = addS 4 (addS 15 emptyS)     -- {4,15}
set3  = listToSet [1,2,3,3,1]       -- {1,2,3}
set4  = listToSet [2,4,8]           -- {2,4,8}


-- Ejercicio 2.2.2: Utilizo un conjunto para quitar los elementos repetidos
sinRepetidos :: Eq a => [a] -> [a]
sinRepetidos xs = setToList (listToSet xs)

-- Función auxiliar muy util:
-- Devuelve un conjunto con los elementos de la lista
-- Ej: ["a","b","b"] -> {"a","b"}
listToSet :: Eq a => [a] -> Set a
listToSet  []    = emptyS
listToSet (x:xs) = x `addS` (listToSet xs)