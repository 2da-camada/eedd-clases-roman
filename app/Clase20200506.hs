module Clase20200506 where

-- Defino los tipos que vamos a usar
data Dir    = Izq | Der deriving (Show)
type Camino = [Dir] 
data Objeto = Tesoro | Chatarra deriving (Show, Eq)

data Mapa = Cofre [Objeto] | Bifurcacion [Objeto] Mapa Mapa deriving (Show)


-- Valores del tipo Mapa
mapa1 = Cofre []
mapa2 = Cofre [Tesoro]
mapa3 = Bifurcacion [Chatarra] (Cofre [Tesoro]) (Cofre []) 
mapa4 = Bifurcacion [Tesoro, Chatarra] 
                (Bifurcacion [Chatarra] 
                        (Cofre [Tesoro]) 
                        (Cofre [Tesoro]))
                (Cofre [])        

-- Valores de tipo Camino (son sólo listas de direcciones)
camino1 = [Izq, Der]
camino2 = [Der, Izq]

----------------- Funciones ----------------

hayTesoro :: Mapa -> Bool
hayTesoro (Cofre objetos) = pertenece Tesoro objetos 
hayTesoro (Bifurcacion objetos mapa1 mapa2) = pertenece Tesoro objetos ||
                                              hayTesoro mapa1 ||
                                              hayTesoro mapa2
{-Ejecucion de hayTesoro mapa3:
    hayTesoro mapa3
    pertenece Tesoro [Chatarra] || hayTesoro Cofre[Tesoro] || hayTesoro Cofre[]
    False  || hayTesoro Cofre[Tesoro] || hayTesoro Cofre[]
    hayTesoro Cofre[Tesoro] || hayTesoro Cofre[] 
    pertenece Tesoro [Tesoro] || hayTesoro Cofre[] 
    True || hayTesoro Cofre[] 
    True 
-}

-- Definir una funcion que cuente la cantidad de cofres que hay en el mapa
contarCantDeCofres :: Mapa -> Int
contarCantDeCofres (Cofre objetos) = 1
contarCantDeCofres (Bifurcacion objetos mapa1 mapa2) =  contarCantDeCofres mapa1 + contarCantDeCofres mapa2 


-- Definir una función que agregue un tesoro a cada cofre
agregarTesoroACofre :: Mapa -> Mapa
agregarTesoroACofre (Cofre objetos) = Cofre (Tesoro : objetos)
agregarTesoroACofre (Bifurcacion objetos mapa1 mapa2) = Bifurcacion objetos (agregarTesoroACofre mapa1) 
                                                                            (agregarTesoroACofre mapa2)


-- Dado un camino ([Dir]) y un Mapa, devuelve los objetos que hay en ese lugar
-- Precondicion: El camino dado debe existir en el Mapa, puede comprobarse si el camino es valido
-- con la funcion esCaminoValido 
objetosEn :: Camino -> Mapa -> [Objeto]
objetosEn [] (Cofre os) = os
objetosEn [] (Bifurcacion os m1 m2) = os
objetosEn (Izq:ds) (Bifurcacion os m1 m2) = objetosEn ds m1
objetosEn (Der:ds) (Bifurcacion os m1 m2) = objetosEn ds m2


-- Determina si se puede realizar ese camino en el Mapa
esCaminoValido :: Camino -> Mapa -> Bool
esCaminoValido [] m = True
esCaminoValido (d:ds) (Cofre xs) = False
esCaminoValido (Izq:ds) (Bifurcacion os m1 m2) = esCaminoValido ds m1
esCaminoValido (Der:ds) (Bifurcacion os m1 m2) = esCaminoValido ds m2


-- Dado un camino ([Dir]) y un Mapa, devuelve los objetos de ese camino 
-- Precondicion: el camino dado debe existir en el Mapa
tomarTodoEnCamino :: Camino -> Mapa -> [Objeto]
tomarTodoEnCamino [] (Cofre os) = os
tomarTodoEnCamino [] (Bifurcacion os m1 m2) = os
tomarTodoEnCamino (Izq:ds) (Bifurcacion os m1 m2) = os ++ tomarTodoEnCamino ds m1 
tomarTodoEnCamino (Der:ds) (Bifurcacion os m1 m2) = os ++ tomarTodoEnCamino ds m2


-- Dado un Mapa, cambia los Cofres por Bifurcaciones con Cofres vacios
reemplazarCofre :: Mapa -> Mapa
reemplazarCofre (Cofre os) = Bifurcacion os (Cofre []) (Cofre [])
reemplazarCofre (Bifurcacion os m1 m2) = Bifurcacion os (reemplazarCofre m1) (reemplazarCofre m2)



--------------------- Funciones auxiliares --------------

pertenece :: Eq a => a -> [a] -> Bool
pertenece x []     = False
pertenece x (y:ys) = x == y || pertenece x ys