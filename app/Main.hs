-- Estructuras de datos
module Main where
import Clase20200318
import Clase20200325
import Clase20200401
import Clase20200408
import Clase20200415
import Clase20200422
import Clase20200429
import Clase20200506
import Clase20200513
import Clase20200520
import Clase20200527
import Clase20200603
import Clase20200610
import Clase20200617
import TAD_Set_ListaSinRepetidos
import TAD_Queue_Listas
import TAD_Stack_Listas
import TAD_Map_ListaSinClavesRepetidas
import TAD_MultiSet_Map
import TAD_Array_Map

main :: IO ()
main = print "Hola Mundo!"

