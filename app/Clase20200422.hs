module Clase20200422 where

type CC = Int
data Item = Plato | Vaso CC | Cuchara | Tenedor | Cuchillo deriving (Show, Eq)
data Mesa = MkMesa [Item] deriving (Show)


-- Definimos algunos valores
mesa1         = MkMesa [Cuchillo, Plato, Plato]
mesaVacia     = ponerMesaPara 0
mesaPara5     = ponerMesaPara 5
mesaMalPuesta = agregarItem Cuchillo mesaPara5


-- Pone un plato, vaso, cuchara, tenedor y cuchillo para cada comenzal dado
ponerMesaPara :: Int -> Mesa
ponerMesaPara n | n < 0     = error "Mesa debe ser mayor que cero"
ponerMesaPara n | otherwise = MkMesa (ponerMesaPara'  n)

ponerMesaPara' :: Int -> [Item]
ponerMesaPara' 0 = []
ponerMesaPara' n = [Plato, Vaso 500, Cuchara, Tenedor, Cuchillo] ++ (ponerMesaPara' (n-1))


-- Calcula la cantidad de un item en una mesa
-- Orden(n)
cantidadDe :: Item -> Mesa -> Int
cantidadDe item (MkMesa items) = apariciones item items

-- Esta es la misma de la practica 1, ejercicio 2.1.8, usando guardas
apariciones :: Eq a => a -> [a] -> Int
apariciones a []                 = 0
apariciones a (x:xs) | a == x    = 1 + apariciones a xs
apariciones a (x:xs) | otherwise =     apariciones a xs 


-- Cantidad de liquido para llenar todos los vasos de una mesa
-- Orden(n): n es el número de items de la mesa
volumenMaximo :: Mesa -> Int
volumenMaximo (MkMesa items) = volumenMaximo' items

volumenMaximo' :: [Item] -> Int
volumenMaximo' []             = 0
volumenMaximo' ((Vaso cc):xs) = cc + volumenMaximo' xs
volumenMaximo' (_:xs)         = volumenMaximo' xs 


-- Agrego un item a una mesa
-- Orden (1)
agregarItem :: Item -> Mesa -> Mesa
agregarItem item (MkMesa items) = MkMesa (item:items)


-- Determina si la mesa no tiene items
-- Orden (1)
mesaEstaVacia :: Mesa -> Bool
mesaEstaVacia (MkMesa []) = True
mesaEstaVacia      _      = False


-- Determina si la mesa esta bien puesta, o sea, si por cada plato, hay un cuchillo, un tenedor y una cuchara
-- Orden(n)
esMesaOrdenada :: Mesa -> Bool
esMesaOrdenada mesa = (cantidadDe Plato mesa == cantidadDe Cuchillo mesa) && 
                      (cantidadDe Plato mesa == cantidadDe Tenedor mesa) &&
                      (cantidadDe Plato mesa == cantidadDe Cuchara mesa)