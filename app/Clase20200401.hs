-- Estructuras de datos. Clase del 01/04/2020
module Clase20200401 where

-- Practica, practica, practica

-- Ejercicio 9 del anexo
agrupar :: Eq a => [a] -> [[a]]
agrupar [] = []
agrupar (x:xs) = (x:tomarIgualesConsec x xs) : agrupar (quitarIgualesConsec x xs)

--Funcion auxiliar: tomo los primeros elementos de una lista que sean iguales al dado
tomarIgualesConsec :: Eq a => a -> [a] -> [a]
tomarIgualesConsec x [] = []
tomarIgualesConsec x (y:ys) = if x == y
                                then y : tomarIgualesConsec x ys
                                else []


--Funcion auxiliar: quito los primeros elementos de una lista que sean iguales al dado
quitarIgualesConsec :: Eq a => a -> [a] -> [a]
quitarIgualesConsec x []     = []
quitarIgualesConsec x (y:ys) = if x == y
                                then quitarIgualesConsec x ys
                                else y:ys


-- Ejercicio 2.2.3 de Recursión sobre números. Se resuelve facilmente resolviendo un problema
--                                             más generico
-- PRECONDICION: N >= 1
contarHasta :: Int -> [Int]
contarHasta n = desdeHasta 1 n

--PRECONDICION:  TOPE >= ACTUAL
desdeHasta :: Int -> Int -> [Int]
desdeHasta actual tope | actual == tope = [actual]
desdeHasta actual tope | otherwise      = actual : desdeHasta (actual+1) tope