module Clase20200429 where

------------------ Comencemos con lo que ya sabemos:

-- Este tipo de datos es equivalente a la lista común y corriente de Haskell
data Lista a = Vacia | DosPuntos a (Lista a) deriving Show

-- Este es un valor de ese tipo
miLista = "hola" `DosPuntos` ("como" `DosPuntos` ("estas" `DosPuntos` Vacia))

-- que es isomorfa (tiene la misma forma) que este valor de lista común de Haskell
listaComun = ["hola", "como", "estas"]

-- Es más, se puede escribir ese isomorfismo con un par de funciones
listaAlist :: Lista a -> [a]
listaAlist Vacia            = []
listaAlist (DosPuntos x xs) = x : listaAlist xs

-- De lista normal a lista mia
listALista :: [a] -> Lista a
listALista []     = Vacia
listALista (x:xs) = DosPuntos x (listALista xs)


-- **Acá se debería ver el pequeño vocabulario sobre árboles binarios**


-- ----------------------------------- ARBOLES BINARIOS

-- Defino cómo son los árboles binarios
data Tree a = Empty | Node a (Tree a) (Tree a) 

-- Para construir los arboles un poco más facilmente
hoja :: a -> Tree a
hoja a = Node a Empty Empty

------------------   Alguno ejemplos de valores tipo Tree


--                  1
--             3        8
--                    2   5
bt1 = Node 1 (hoja 3)
             (Node 8 (hoja 2)
                     (hoja 5)) :: Tree Int


--                 4
--           2            6
--        1    3       5    7
bt2 = Node 4 (Node 2 (Node 1 Empty Empty)
                     (Node 3 Empty Empty))
             (Node 6 (Node 5 Empty Empty)
                     (Node 7 Empty Empty)) :: Tree Int


--                "hola"
--           "adios"     "chau"          
bt3 = Node "hola" (Node "adios" Empty Empty)
                 (Node "chau"  Empty Empty)


--              1
--                 2
--                    3
bt4 = Node 1 Empty (Node 2 Empty (Node 3 Empty Empty)) :: Tree Int


--                          8
--                  3             10
--             1       6               14
--                  4    7          13                 
bst  = Node 8 bst1 bst2
bst1 = Node 3 (hoja 1) (Node 6 (hoja 4) (hoja 7))
bst2 = Node 10 Empty (Node 14 (hoja 13) Empty)

--------------- De listas a arboles

-- Todos los maps se pueden pensar como funciones que preservan las estructuras
-- Un map de una lista dará una nueva lista con la misma cantidad de elementos
-- Un ma en un arbol dará un nuevo arbol con la misma estructura y cantidad de nodos

transformar :: String -> Int
transformar str = length str

mapSobreLista :: Lista String -> Lista Int
mapSobreLista Vacia            = Vacia
mapSobreLista (DosPuntos x xs) = DosPuntos (transformar x) (mapSobreLista xs)

mapSobreArboles :: Tree String -> Tree Int
mapSobreArboles Empty          = Empty
mapSobreArboles (Node x t1 t2) = Node (transformar x) (mapSobreArboles t1) (mapSobreArboles t2)   

-- Notar: que la unica diferencia importante es que el map de arboles hay dos
-- recusiones, y en la de lista una sola.


----------------------------- Practica 3 ------------------------------------

sumarT :: Tree Int -> Int
sumarT Empty          = 0
sumarT (Node n t1 t2) = n + (sumarT t1) + (sumarT t2) 


mapDobleT :: Tree Int -> Tree Int
mapDobleT Empty = Empty
mapDobleT (Node n t1 t2) = Node (n*2) (mapDobleT t1) (mapDobleT t2)


leaves :: Tree a -> [a]
leaves Empty          = []
leaves (Node a t1 t2) = if (esHoja (Node a t1 t2))
                           then [a]
                           else leaves t1 ++ leaves t2

esHoja :: Tree a -> Bool
esHoja (Node _ Empty Empty) = True
esHoja _                    = False


-- Listados inorder, preorder, posorder
listInOrder :: Tree a -> [a]
listInOrder Empty          = []
listInOrder (Node a t1 t2) = listInOrder t1 ++ [a] ++ listInOrder t2

listInPreOrder :: Tree a -> [a]
listInPreOrder Empty          = []
listInPreOrder (Node a t1 t2) =  [a] ++ listInPreOrder t1 ++ listInPreOrder t2

listInPosOrder :: Tree a -> [a]
listInPosOrder Empty          = []
listInPosOrder (Node a t1 t2) =  listInPosOrder t1 ++ listInPosOrder t2 ++ [a]





















-------------- Un show para arboles binarios ----------------------
-- Permite representar los arboles más graficamente
instance (Show a) => Show (Tree a) where
   show t = unlines (prettyPrint t)

prettyPrint :: Show a => Tree a -> [String]
prettyPrint Empty                  = [""]
prettyPrint (Node e Empty Empty) = [show e]
prettyPrint (Node e ti td)       = show e : prettyPrintSubtree ti td

prettyPrintSubtree :: Show a => Tree a -> Tree a -> [String]
prettyPrintSubtree ti td  = padTree "├──" "│  " (prettyPrint ti) ++
                            padTree "└──" "   " (prettyPrint td)

padTree :: String -> String -> [String] -> [String]
padTree first rest = zipWith (++) (first : repeat rest)
