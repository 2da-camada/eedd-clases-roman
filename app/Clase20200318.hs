-- Estructuras de datos. Clase del 18/03/2020
module Clase20200318 where

{- Stack es un programa que crea un ambiente donde progamar en Haskell.
   Se pueden ejecutar los siguientes comandos estando en un proyecto stack:

 stack run: Ejecuta el proyecto. Debería decir "Hola Mundo!"
 stack ghci: Ejecuta el interprete con los archivos del proyecto

 Estando dentro del interprete ghci (Glasgow Haskell Compiler Interpreter)
 se pueden evaluar expresiones:

 > 3 + 5
 8

 > True && False
 False

 > 34 + (4 < 3)
 error!...

Le podemos indicar que recargue los archivos
> :r

Le podemos pedir el tipo de una expresión:
 > :t mod
 mod :: Integral a => a -> a -> a

Aquí nos dice que mod toma dos números de la familia
de los enteros y devuelve otro de esos números

Para terminar
> :q
-}

f :: Int -> Int
f x = x + 3

g :: Int -> Int
g x = x * 5

esPar :: Int -> Bool
esPar x = mod x 2 == 0

-- Pregunta: Cual es la diferencia entre "A" y 'A'????
-- Respuesta: "A" es un String y 'A' es un Char

-- Esto no compila. Que tipo tendria?
-- mal x = (x > 3) + 2

valorAbsoluto :: Int -> Int
valorAbsoluto x = if (x >0) then x else -x  

minimo :: Int -> Int -> Int
minimo x y = if x < y then x else y

maximo :: Int -> Int -> Int
maximo x y = if x > y then x else y

minimo3 :: Int -> Int -> Int -> Int
minimo3 x y z = minimo (minimo x y) z

maximo3 :: Int -> Int -> Int -> Int
maximo3 x y z = maximo (maximo x y) z

medio3 :: Int -> Int -> Int -> Int
medio3  x y z = if x /= (maximo3 x y z) && x /= (minimo3 x y z)
                then x
                else if (y /= (maximo3 x y z) && y /= (minimo3 x y z)) 
                     then y
                     else z


medio3' x y z = if (x > y && x < z) || (x>z && x < y)
                then x
                else if (y>x && y< z) || (y>z && y < x)
                     then y
                     else z 



-- TAREA PARA HACER MAÑANA CON LUZ: Practica 1. Punto 1.
-- Definir: sucesor, sumar y maximo.

toBool :: Int -> Bool
toBool 0 = False
toBool _ = True

saludar :: String -> String
saludar "Juan"  = "Hola, como estas Juan?"
saludar "Pedro" = "Che, acordate que me debes $100"
saludar otro    = "Buenas, " ++ otro


masPatternMatching :: Int -> String
masPatternMatching 0    = "Es cero"
masPatternMatching (-1) = "Es menos uno"
masPatternMatching (1)  = "Es uno"
masPatternMatching _    = "No es ni 1, -1, ni cero"
