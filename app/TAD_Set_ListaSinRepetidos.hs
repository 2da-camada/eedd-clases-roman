module TAD_Set_ListaSinRepetidos (Set, emptyS, addS, belongS, sizeS, removeS, unionS, intersectionS, setToList) where

-- OJO: ESTA NO ES LA IMPLEMENTACION QUE SE REQUIERE EN LA PRACTICA 4

-- Defino el tipo de dato de la implementacion
-- INVARIANTE: La lista contenida en Set no tiene elementos repetidos
data Set a = Set [a]



-- Devuelvo un conjunto vacio
emptyS :: Set a
emptyS = Set []


-- Agrego un elemento al conjunto
addS :: Eq a => a -> Set a -> Set a
addS x (Set xs) = if (x `elem` xs )
                     then Set xs
                     else Set (x:xs)


-- Determina si un elemento pertenece al conjunto
belongS :: Eq a => a -> Set a -> Bool
belongS x (Set xs) = x `elem` xs


-- Devuelve la cantidad de elementos distintos de un conjunto
sizeS :: Eq a => Set a -> Int
sizeS (Set xs) = length xs


-- Borra un elemento del conjunto
removeS :: Eq a => a -> Set a -> Set a
removeS x (Set xs) = Set (borrarPrimer x xs)


-- Dados dos conjuntos devuelve un conjunto con todos los elementos de ambos conjuntos
unionS :: Eq a => Set a -> Set a -> Set a
unionS (Set xs) (Set ys) = Set (unionSinRepetidos xs ys)


-- Dados dos conjuntos devuelve un conjutno con todos los elementos en común entre ambos
intersectionS :: Eq a => Set a -> Set a -> Set a
intersectionS (Set xs) (Set ys) = Set (elementosComunes xs ys)


-- Devuelve una lista con los elementos del conjunto
setToList :: Eq a => Set a -> [a]
setToList (Set xs) = xs


-------------- Funciones auxiliares

-- Borra el primer elemento igual al dado en la lista
borrarPrimer :: Eq a => a -> [a] -> [a]
borrarPrimer x   []   = []
borrarPrimer x (y:ys) = if x == y
                          then ys
                          else y : borrarPrimer x ys 


unionSinRepetidos :: Eq a => [a] -> [a] -> [a]
unionSinRepetidos   []   ys = ys
unionSinRepetidos (x:xs) ys = if (x `elem` ys)
                                 then unionSinRepetidos xs ys
                                 else unionSinRepetidos xs (x:ys)


-- Calculo los elementos comunes entre dos listas. Supongo no hay repetidos
elementosComunes :: Eq a => [a] -> [a] -> [a]
elementosComunes xs ys = elementosComunes' xs ys []

elementosComunes' :: Eq a => [a] -> [a] -> [a] -> [a]
elementosComunes' []     ys res = res
elementosComunes' (x:xs) ys res = if x `elem` ys
                                     then elementosComunes' xs ys (x:res)
                                     else elementosComunes' xs ys res 





















---------- No dar importancia, es para que se muestren más lindo ------
instance Show a => Show (Set a) where
    show (Set xs) = "{"++ mostrarItems xs ++ "}"

mostrarItems :: Show a => [a] -> String
mostrarItems    []    = ""
mostrarItems    [x]   = show x
mostrarItems (x:y:ys) = show x ++ "," ++ mostrarItems (y:ys)
