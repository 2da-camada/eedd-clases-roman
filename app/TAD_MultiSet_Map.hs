module TAD_MultiSet_Map (MultiSet, emptyMS, addMS, ocurrencesMS, multiSetToList) where
import TAD_Map_ListaSinClavesRepetidas

-- Definimos el tipo algebraico
-- INVARIANTE DE REPRESENTACION: No debe haber en el map asociaciones con cero cantidad de veces
data MultiSet a = MultiSet (Map a Int)




-- Crea un multiconjunto vacío.
emptyMS :: MultiSet a 
emptyMS = MultiSet emptyM


-- Dados un elemento y un multiconjunto, agrega una ocurrencia de ese elemento al multiconjunto.
addMS :: Ord a => a -> MultiSet a -> MultiSet a 
addMS x (MultiSet map) = if veces == 0              
                            then MultiSet (assocM x     1     map)  -- Si no estaba, ahora esta 1 vez
                            else MultiSet (assocM x (veces+1) map)  -- Si estaba, ahora esta una vez más
                         where veces = maybeIntToInt (lookupM map x) 



-- Dados un elemento y un multiconjunto indica la cantidad de apariciones de ese elemento en el multiconjunto.
ocurrencesMS :: Ord a => a -> MultiSet a -> Int 
ocurrencesMS x (MultiSet map) = maybeIntToInt (lookupM map x)


-- Dado un multiconjunto devuelve una lista con todos los elementos del conjunto y su cantidad de ocurrencias.
multiSetToList :: Ord a => MultiSet a -> [(Int,a)]
multiSetToList (MultiSet map) = listarMultiSet (domM map) (MultiSet map)

listarMultiSet :: Ord a => [a] -> MultiSet a -> [(Int,a)]
listarMultiSet   []   mset = []
listarMultiSet (x:xs) mset = (ocurrencesMS x mset,x) : listarMultiSet xs mset


-- NOTA1: falta implementar las funciones unionMS e intersectionMS
-- NOTA2: Faltaría algún método para quitar elementos del multiset (removeMS)


------------ Funciones auxiliares

-- Dado un valor Maybe Int devuelve un Int. Si no está devuelve cero, sino el número
maybeIntToInt :: Maybe Int -> Int
maybeIntToInt Nothing  = 0
maybeIntToInt (Just n) = n



















---------- No dar importancia, es para que se muestren más lindo ------
instance (Show a, Eq a, Ord a) => Show (MultiSet a) where
  show ms = "{{" ++ mostrarItems items ++ "}}"
            where items = concatMap (\(n,x) -> replicate n x) (multiSetToList ms)

mostrarItems :: Show a => [a] -> String
mostrarItems    []    = ""
mostrarItems    [x]   = show x
mostrarItems (x:y:ys) = show x ++ "," ++ mostrarItems (y:ys)
