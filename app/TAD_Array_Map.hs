module TAD_Array_Map(Array, emptyA, sizeA, getA, setA) where
import TAD_Map_ListaSinClavesRepetidas
import Data.Maybe

data Array a = Array (Map Int a) Int deriving Show

--Crea un array vacío de una cantidad de elementos dado inicializados con un valor dado. Las posiciones comienzan en cero.
emptyA :: Int -> a -> Array a
emptyA n val = Array (initMap (n-1) val emptyM) n

initMap :: Int -> a ->  Map Int a -> Map Int a
initMap 0 val map = assocM 0 val map
initMap n val map = assocM n val (initMap (n-1) val map)


-- Devuelve el tamaño del arreglo
sizeA :: Array a -> Int
sizeA (Array map max) = max  


--Devuelve el elemento almacenado en la posición dada. Si se pasa una posición inválida devuelve un error.
getA :: Array a -> Int -> a
getA (Array map max) pos = if (pos >= max) || (pos <0)
                            then error "Indice inválido"
                            else fromJust (lookupM map pos)


--Pone en la posición dada el elemento pasado por parámetro. Si se pasa una posición inválida devuelve un error.
setA :: Array a -> Int -> a -> Array a
setA (Array map max) pos val = if (pos >= max) || (pos <0)
                              then error "Indice inválido"
                              else Array (assocM pos val map) max
