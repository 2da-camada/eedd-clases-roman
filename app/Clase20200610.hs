module Clase20200610 where
import Clase20200429

{- Hoy vamos a dedicarnos a hacer el borrado de un arbol binario de búsqueda.
   Para ello vamos a ir haciendo algunos ejercicios de la práctica que nos brindarán
   herramientas y formas de analizar los posibles casos.


* Primero: Aprendamos a buscar un elemento en el BST    

Busco siguiendo las invariantes: el elemento buscado es el nodo actual, o en caso contrario,
                                 si es menor debe estar en el hijo izquierdo, sino en el derecho.
-}

--Dado un BST dice si el elemento pertenece o no al árbol
perteneceBST :: Ord a => a -> Tree a -> Bool
perteneceBST x Empty = False
perteneceBST x (Node e t1 t2) = if x ==e
                                   then True
                                   else if x < e
                                           then perteneceBST e t1
                                           else perteneceBST e t2 

{-
* Segundo: para el momento de cambiar la estructura del arbol, debemos saber cómo encontrar
           el menor (o mayor) de un arbol. 
            - El menor será el elemento que se encuentre "más a la izquiera"
            - El mayor será el elemento que se encuentre "más a la derecha"
-}

-- Dado un BST devuelve el mínimo elemento
minBST :: Ord a => Tree a -> a
minBST       Empty      = error "El árbol está vacío"
minBST (Node n Empty _) = n             -- Lo encontré, no puedo ir más a la izq porque es vacío
minBST (Node n t1 t2)   = minBST t1     -- Puedo ir a la izq, así que continuo


{-
* Tercero: Nos va a resultar útil saber cómo eliminar el menor valor, que va a estar en la rama 
           más izquierda
-}

-- Dado un BST devuelve el árbol sin el mínimo elemento
deleteMinBST :: Ord a => Tree a -> Tree a
deleteMinBST        Empty      = Empty
deleteMinBST (Node n Empty t2) = t2      -- Lo encontré (ya no puedo ir a la izq. Lo borro, o sea no lo devuelvo)
deleteMinBST (Node n t1 t2)    = Node n (deleteMinBST t1) t2 -- Sigo la recursión, porque hay algo más a la izq


{-
* Cuarto: aprendamos a distinguir todos los posibles casos para de un BST (aparte de
          ser Empty o de ser Node)
          Caso 1: Es vacío
          Caso 2: Es una hoja (un nodo sin hijos, o mejor dicho, sus hijos son Empty)
          Case 3: Es un nodo con un solo hijo, el izquierdo (o sea que el hijo derecho es Empty)
          Caso 4: Es un nodo con un solo hijo, el derecho (o sea que el hijo izquierdo es Empty)
          Caso 5: Es un nodo que tiene dos hijos (ninguno de los dos hijos es Empty)
-}


-- Utilicemos esta función auxiliar, para escribirlo de forma "linda"
node :: Tree a -> a
node (Node x t1 t2) = x


esBST :: Ord a => Tree a -> Bool
esBST       Empty          = True                                                   -- Caso 1
esBST (Node n Empty Empty) = True                                                   -- Caso 2
esBST (Node n t1 Empty)    = (n > node t1) && esBST t1                              -- Caso 3
esBST (Node n Empty t2)    = (n < node t2) && esBST t2                              -- Caso 4
esBST (Node n t1 t2)       = (n > node t1) && esBST t1 && (n < node t2) && esBST t2 -- Caso 5



{-
* Quinto: por fin vamos a poder borrar un elemento cualquiera del arbol

  - En la función deleteBST utilizamos la estrategia del perteneceBST para buscar el elemento que
    debemos borrar. Si lo encontramos, debemos rearmar ESE subarbol utilizando los hijos izquierdo
    y derecho del nodo encontrado.
  - Rearmamos el nuevo subarbol, contemplando los casos
      Caso a) Los dos hijos son vacíos, o sea que debo devolver un árbol vacío
      Caso b) Tiene un sólo hijo: el hijo debe ser el nuevo árbol
      Caso c) Tiene dos hijos. El valor del nuevo nodo se puede calcular de dos formas diferentes:
              c1) Puede ser el menor de los mayores
              c2) Puede ser el mayor de los menores
   
  En esta implementación elegimos el "menor de los mayores". Los mayores son los hijos derechos.
-}


-- Dado un BST borra un elemento en el árbol
deleteBST :: Ord a => a -> Tree a -> Tree a
deleteBST a      Empty     = Empty 
deleteBST a (Node n t1 t2) = if (a == n)
                               then rearmarBST t1 t2
                               else if (a > n)
                                      then (Node n t1 (deleteBST a t2))
                                      else (Node n (deleteBST a t1) t2)

-- Función auxiliar: rearmo el subarbol del que se debe borrar el valor del nodo
rearmarBST :: Ord a => Tree a -> Tree a -> Tree a
rearmarBST Empty Empty = Empty
rearmarBST   t1  Empty = t1
rearmarBST Empty  t2   = t2
rearmarBST   t1   t2   = Node (minBST t2) t1 (deleteMinBST t2)

