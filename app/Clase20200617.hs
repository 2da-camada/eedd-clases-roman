module Clase20200617 where
import TAD_Array_Map
import TAD_Stack_Listas

-- Punto 1: La implementación del TAD Array (con Map) está en el archivo TAD_Array_Map
array1 = emptyA 3 0
array2 = setA (emptyA 3 100) 0 200

-- Punto 2: Definir el tipo algebraico Muestra

data Muestra = Muestra Nombre (Array Int) deriving Show
type Nombre = String

-- Definir cuatro muestras
muestra1 = Muestra "Juan" (emptyA 3 600)
muestra2 = Muestra "Pedro" array2

-- Una muestra es válida si cada analisis está dentro del +-10% del promedio
esValida :: Muestra -> Bool
esValida (Muestra _ a) = (getA a 0) `estaEntre` lims &&
                         (getA a 1) `estaEntre` lims &&
                         (getA a 2) `estaEntre` lims 
            where lims      = (promedio - variacion, promedio + variacion)  -- limite inferior y superior
                  promedio  = promedioArray3 a
                  variacion = (promedio * 10) `div` 100 
                  
 


-- Una muestra es positiva si el promedio es mayor a 500
esPositiva :: Muestra -> Bool
esPositiva (Muestra _ a) = promedioArray3 a > 500 



-- PUNTO 3: Definir una pila

pilaMuestras1 = push muestra2 (push muestra1 emptyST)
pilaMuestras2 = push muestra1 (push muestra2 (push muestra1 emptyST))

-- Una pila de muestras es peligrosa si la mayoria de las muestras es positiva
esPilaPeligrosa :: Stack Muestra -> Bool
esPilaPeligrosa pila = cantPositivas > (cantTotal `div` 2)
  where cantTotal     = contarElementos pila
        cantPositivas = contarMuestrasPositivas pila



----------------- Funciónes auxiliares --------------------------

-- Devuelve true si un numero está entre dos numeros dados, inclusive
estaEntre :: Int -> (Int,Int) -> Bool
estaEntre n (li,ls)  = n >= li && n <= ls


-- Devuelve el promedio de un array de tres elementos
promedioArray3 :: Array Int -> Int
promedioArray3 a = ((getA a 0) + (getA a 1) + (getA a 2)) `div` 3


-- Cuenta cuantos elementos tiene una pila
contarElementos :: Stack a -> Int
contarElementos p = if (isEmptyST p)
                      then 0
                      else 1 + contarElementos (pop p)


-- Cuenta cuantas muestras positivas hay en una pila
contarMuestrasPositivas :: Stack Muestra -> Int
contarMuestrasPositivas p = if (isEmptyST p)
                            then 0 
                            else if esPositiva(top p)
                                 then 1 + contarMuestrasPositivas (pop p)
                                 else 0 + contarMuestrasPositivas (pop p)