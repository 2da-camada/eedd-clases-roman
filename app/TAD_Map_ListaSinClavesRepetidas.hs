module TAD_Map_ListaSinClavesRepetidas (Map, emptyM, assocM, lookupM, deleteM, domM) where

-- Implemento un Map (también conocido como diccionario) como una lista de pares (clave,valor)
-- El par (clave,valor) también se lo conoce como asociación
-- INVARIANTE DE REPRESENTACION: La lista no va a tener elementos con claves repetidas
data Map k v = Map [(k,v)]



-- Devuelvo un Map vacio (no tiene ninguna asociación)
emptyM :: Map k v
emptyM = Map []


-- Agrego una asociacion al map. Si la clave ya está, la piso.
assocM :: Eq k => k -> v -> Map k v -> Map k v
assocM k v (Map xs) = Map (agregar k v xs)

agregar :: Eq k => k -> v -> [(k,v)] -> [(k,v)]
agregar k v [] = [(k,v)]
agregar k v ((k',v'):xs) = if k == k' 
                            then (k,v):xs
                            else (k',v') : agregar k v xs

                         
-- Busco el valor asociado a una clave en el map. Si no está devuelvo (Nothing), si está devuelvo (Just valor)                         
lookupM :: Eq k => Map k v -> k -> Maybe v
lookupM      (Map []) _      = Nothing
lookupM (Map ((k1,v1):xs)) k = if k == k1
                                  then Just v1
                                  else lookupM (Map xs) k


-- Borro una asociación con la clave dada
deleteM :: Eq k => Map k v -> k -> Map k v
deleteM (Map xs) k = Map (borrar k xs)

borrar :: Eq k => k -> [(k,v)] -> [(k,v)]
borrar      _ []      = []
borrar k ((k1,v1):xs) = if (k == k1)
                           then xs
                           else (k1,v1) : borrar k xs


-- Devuelvo una lista con todas las claves (no deberia haber claves repetidas).
-- No tiene ningún orden especificado, por lo que puede cambiar el orden de los elementos
-- en las diferentes implementaciones.
domM :: Map k v -> [k]
domM      (Map [])    = []
domM (Map ((k,v):xs)) = k : domM (Map xs)





---------- No dar importancia, es para que se muestren más lindo ------
instance (Show k, Show v) => Show (Map k v) where
    show (Map xs) = "{"++ mostrarAsociaciones xs ++ "}"

mostrarAsociaciones :: (Show k, Show v) => [(k,v)] -> String
mostrarAsociaciones    []     = ""
mostrarAsociaciones    [kv]   = mostrarAsociacion kv
mostrarAsociaciones (kv:y:ys) = mostrarAsociacion kv ++ ", " ++ mostrarAsociaciones (y:ys)

mostrarAsociacion :: (Show k, Show v) => (k,v) -> String
mostrarAsociacion (k,v) = show k ++ "->" ++ show v