module Clase20200408 where

-- TIPOS DE DATOS ALGEBRAICOS

-- Tipos Primitivos: universo de valores posibles: Bool, Char, Int, Tuplas, Listas
-- Constructores y Pattern Matching

-- Bool = {True, False}                                (2 elementos)
-- Char = {'a', 'z', '1', ' ', '*´}                    (2^16 =65536 elementos)
-- Int  = {-2000000000.... 200000000}                  (2^64 elementos)
-- Tuplas: (Bool, Bool)                                (4 elementos)

-- Tuplas: (Bool, Bool, Char)                          (2^18)                                             
--           2  *  2   * 65536                             

-- Ejemplos de pattern matching: Siempre se hace pattern matching sobre los valores en los tipos simples
-- (Bool, Int, Char) o sobre la estructura en los tipos complejos (tuplas, listas)
pmsb :: Bool -> Int
pmsb True  = 1
pmsb False = 99

pmsn :: Int -> Int
pmsn 77    = 177
pmsn (-23) = 8

pmst :: (Int, Bool) -> Bool
pmst (a,b) = if a == 10 
                then b
                else not b

pmsl :: [Int] -> Int
pmsl []     = 0
pmsl (x:xs) = 1


-- Creo un tipo NUEVO enumerando los posibles valores que puede tener
data Color = Negro | Azul | Verde | Amarillo

-- El nuevo tipo se usa en funciones a traves de pattern matching
pmsc :: Color -> String
pmsc Negro = "Negro"
pmsc Azul = "Azul"
pmsc Verde = "Verde"
pmsc Amarillo = "Amarillo"


-- Creo otro tipo nuevo
data Cliente = Clie String Int

-- Creamos algunos valores de este tipo

pedro = Clie "Pedro" 20
juan  = Clie "Juan"  33

-- Y una función que los usa

edad :: Cliente -> Int
edad (Clie nom anios) = anios

-- Aquí "Clie" es un constructor de valores de tipo Cliente
-- Clie es una función que toma un String y número y devuelve un cliente
-- Si tipeamos ":t Clie", se verá que dice "Clie :: String -> Int -> Cliente"


-- Definimos otro tipo nuevo. Es tán nuevo que no sabe ni como mostrarse
-- para que sepa mostrarse se debe agregar el "deriving(Show)" a la definición del tipo
data Ingrediente = Salsa | Queso | Jamon | Aceitunas Int deriving(Show)

-- Construimos algunos valores del tipo Ingrediente
i1 = Salsa
i2 = Queso
i3 = Jamon
i4 = Aceitunas 6


-- Construimos una funcion de determina si un ingrediente es caro: (Queso y Jamon)
esCaro :: Ingrediente -> Bool
esCaro Queso = True
esCaro Jamon = True
esCaro  _    = False

-- Se pueden mezclar los nuevos tipos con los tipos ya conocidos:
-- Hagamos una lista de ingredientes
listaIngredientes = [Jamon, Queso, Aceitunas 3, Queso ]


-- Funciona que calcula la cantidad de ingredientes caros de una lista
cantDeCaros :: [Ingrediente] -> Int
cantDeCaros []     = 0
cantDeCaros (i:is) = if esCaro i
                        then 1 + cantDeCaros is
                        else 0 + cantDeCaros is



-- Definimos otro tipo nuevo.
-- Este es más complicado porque es recursivo ("Pizza" aparece tanto a la izquierda como a la 
-- derecha de la definición del tipo)
-- Es semejante a una lista: La pizza es una prepizza o es una capa con ingrediente y otra pizza
data Pizza = Prepizza | Capa Ingrediente Pizza deriving (Show)

-- Construimos valores del tipo Pizza
pizza1 = Prepizza
pizza2 = Capa Salsa Prepizza
pizza3 = Capa Queso (Capa Jamon pizza2)
pizza4 = Capa (Aceitunas 5) (Capa Salsa (Capa (Aceitunas 3) Prepizza)) 


-- Capa Queso (Capa Salsa (Capa Salsa Prepizza))  -- Esto es la que dice Haskell
-- Capa Queso (Capa Salsa        (pizza 2)     )
-- Capa Queso (Capa Salsa (Capa Salsa Prepizza))

-- Función que cuente la cantidad de aceitunas que tiene una pizza
cantAceitunas :: Pizza -> Int
cantAceitunas Prepizza = 0
cantAceitunas (Capa (Aceitunas n) pizza) = n + cantAceitunas pizza
cantAceitunas (Capa ingrediente  pizza)  = 0 + cantAceitunas pizza

--------- Sinonimos de tipos, tipos algebraicos y los sospechosos de siempre ---------------

-- Defino un tipo "enumerativo"
data Dia = Lunes | Martes | Miercoles | Jueves | Viernes | Sabado | Domingo deriving (Show)

-- Queremos que sepa comparar dos materias, se le agrega "deriving (Eq)"
-- Otro tipo enumerativo
data Materia = Estructuras | Matematica deriving(Show, Eq)

-- Determinar si un dia es fin de semana
esFinDeSemana :: Dia -> Bool
esFinDeSemana Sabado  = True
esFinDeSemana Domingo = True
esFinDeSemana    _    = False

-- Nuevo tipo: Horarios
-- OJO:: EL "Horario" de la izquierda es un tipo y el "Horario" de la derecha
--       es un constructor.
data Horario = Horario Materia Dia Int Int deriving(Show)


-- Construyo valores del tipo Horario
eedd1 = Horario Estructuras Miercoles 17 21
eedd2 = Horario Estructuras Sabado 9 13
mate1 = Horario Matematica Lunes 17 21
mate2 = Horario Matematica Viernes 17 21

-- OJO!!! ACA ESTOY HACIENDO UN SINONIMO DE TIPO: NO ES UN TIPO NUEVO
type Agenda = [Horario]

miAgenda = [eedd1, eedd2, mate1, mate2]

-- Se pueden combinar los nuevos tipos con los tipos ya vistos
horariosDeMaterias :: Agenda -> Materia -> [Horario]
horariosDeMaterias []                     m' = []
horariosDeMaterias ((Horario m d i f):hs) m' = if (m == m')
                                                  then Horario m d i f : horariosDeMaterias hs m' 
                                                  else                   horariosDeMaterias hs m'

-- Ejercicios: Se puede hacer la practica 2 hasta el ejercicio de la Pizza inclusive