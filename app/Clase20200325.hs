-- Estructuras de datos. Clase del 25/03/2020
module Clase20200325 where

---------------- RECURSION SOBRE NUMEROS  ----------------------

-- Multiplicar es sumar m veces el numero n
-- Por ejemplo: 3 x 4 = 3 + 3 + 3 + 3 = 12
multiplicar :: Int -> Int -> Int
multiplicar n 0 = 0
multiplicar n m = n + multiplicar n (m-1)

{- Ejecución de multiplicar 3 4:
    multiplicar 3 4
    3 + multiplicar 3 3
    3 + 3 + multiplicar 3 2
    6 + multiplicar 3 2
    6 + 3 + multiplicar 3 1
    9 + multiplicar 3 1
    9 + 3 + multiplicar 3 0
    12 + multiplicar 3 0
    12 + 0
    12
-}

-- Factorial de n es la multiplicacion de todos los enteros desde 1 hasta n 
-- factorial 4 = 1 * 2 * 3 * 4 = 24
-- factorial 4 = 4 * 3 * 2 * 1 = 24
-- factorial 5 = 1 * 2 * 3 * 4 * 5 = 24 * 5 = 120
factorial :: Int -> Int
factorial 1 = 1
factorial n = n * factorial (n-1)

{- Ejecución del factorial de 3:
    factorial 3
    3 * factorial 2
    3 * 2 * factorial 1
    6 * factorial 1
    6 * 1
    6
-}

-- https://es.wikipedia.org/wiki/Sucesi%C3%B3n_de_Fibonacci
fibonacci :: Int -> Int
fibonacci 0 = 0
fibonacci 1 = 1
fibonacci n = fibonacci (n-1) + fibonacci (n-2)


{- OPCIONAL: Si alguno quiere leer sobre la Conjetura de Collatz 
  https://es.wikipedia.org/wiki/Conjetura_de_Collatz -}
 
collatz :: Int -> Int
collatz 1 = 1
collatz n = collatz (collatzStep n)

collatzStep :: Int -> Int
collatzStep n | n `mod` 2 == 0 = n `div` 2
              | otherwise      = 3 *n +1

{- La primera estructura de datos vistas son las tuplas:
  - Las tuplas tienen una cantidad determinada de componentes
  - Cada componente puede ser de un tipo diferente
-}


{------------------------- LISTAS --------------------
 Es la segunda estructura de datos que vemos:
   - Las listas pueden tener cualquier cantidad de elementos
   - Todos los elementos son del mismo tipo

 Todas las listas tienen alguna de las siguientes formas:
   - []     : es una lista vacía
   - (x:xs) : es una lista que tiene un elemento adosado a una lista

 NOTA: La listas se definen recursivamente.

  Para construir listas se usan [] y ":" como constructores   
-}

-- Ejemplos de listas

miLista1 :: [Int]
miLista1 = [1,3,4]

miLista1' :: [Int]
miLista1' = 1 : (3 : (4 : []))
--             --------------
--           x :    lista

listaVacia = []
listaDeUnElemento = [66]
listaDeUnElemento' = 66 : []
listaConStrings = ["Hola","como","estas"]
listaConStrings' = "Hola" : "como" : "estas" : []


{- Pequeño desvio: Sinonimos de tipos

-- Definimos un sinonimo de tipo. Son el mismo tipo
type Direccion = (String, String, Int)

direccion = ("PBA", "Vieytes", 337)

calle :: Direccion -> Int
calle (_, _ , n) = n

-- Si tipeamos :t "Hola" el REPL devuelve [[Char]]
-- Un String es una Lista de caracteres!!!
-- Es un sinomimo del tipo [[Char]]
-}


-- Escribir [3,7] con los constructores
lista37  =  3 : (7 : [])
lista37' =  3 :   [7]

-- Determinar si una lista está vacia. Ejercicio 1.1.4.a
isEmpty :: [a] -> Bool
isEmpty [] = True
isEmpty  _ = False

-- Devolver el primer elemento de una lista. Ejercicio 1.1.4.b
-- Head es una función parcial. No está definida en todo su dominio
-- en particular, no está definida head' []
head' :: [a] -> a
head'(x:xs) = x


----------------- Ejercicios inventados de listas y tuplas ----------------------

-- Dada una lista no vacía devolver un par donde la primer componente es el primer elemento
-- de la lista y la segunda componente es el resto de la lista
-- ejemplo1 [1,2,3] = (1,[2,3])
ejemplo1 :: [a] -> (a,[a])
ejemplo1 (x:xs) = (x,xs)

-- Hacer una función que dada una lista de numeros devuelva una tripleta con
-- la cantidad de elementos, la suma y el promedio
-- ejemplo2 [10, 10] = (2,20,10)
ejemplo2 :: [Int] -> (Int,Int,Int)
ejemplo2 xs = (longitud xs, sumatoria xs, promedio xs)


------------------------ FUNCIONES RECURSIVAS SOBRE LISTAS ----------------

{- Primera funcion recursiva sobre listas -}

-- Calcular la longitud de la lista. Ejercicio 1.2.1.2
longitud :: [a] -> Int
longitud []     = 0
longitud (_:xs) = 1 + longitud xs

{- Ejecución de longitud [3, 7, 2]
    longitud [3,7,2]
    1 + longitud [7,2]
    1 + 1 + longitud [2]
    1 + 1 + 1 + longitud []
    1 + 1 + 1 + 0
    3
-}

-- Dada una lista de numeros, calcular la suma de todos ellos. Ejercicio 1.2.1.1
sumatoria :: [Int] -> Int
sumatoria []        = 0
sumatoria (n:resto) = n + sumatoria resto


-- Dada una lista de números, calcular el promedio. Ejercicio 1.2.1.20
promedio :: [Int] -> Int
promedio xs = div (sumatoria xs) (longitud xs)

-- OPCIONAL: Este promedio si muestra las comas
promedioConComa :: [Int] -> Float
promedioConComa xs = fromIntegral(sumatoria xs) / fromIntegral(longitud xs)

-- Ejercicio 1.2.1.3
mapSucesor :: [Int] -> [Int]
mapSucesor []     = []
mapSucesor (x:xs) = x+1 : mapSucesor xs

{- Ejecución de mapSucesor [3,4]
    mapSucesor [3,4]
    3+1 : mapSucesor [4]
    4 : mapSucesor [4]
    4 : 4 +1 : mapSucesor []
    4 : 5 : mapSucesor []
    4 : 5 : []
    [4,5]
-}

-- Ejercicio 1.2.1.9
-- Ejemplo losMenoresA 5 [3,8,15,2] = [3,2]
losMenoresA :: Int -> [Int] -> [Int]
losMenoresA n [] = []
losMenoresA n (x:xs) = if x < n then x : losMenoresA n xs else losMenoresA n xs

{- Ejecución de losMenoresA 5 [3,8,15,2]:
    losMenoresA 5 [3,8,15,2]
    if 3 < 5 then 3 : losMenoresA 5 [8,15,2] else losMenoresA 5 [8,15,2]
    3 : losMenoresA 5 [8,15,2]
    3 : if 8 < 5 then 8 : losMenoresA 5 [15,2] else losMenoresA 5 [15,2]
    3 : losMenoresA 5 [15,2]
    3 : if 15 < 5 then 15 : losMenoresA 5 [2] else losMenoresA 5 [2]
    3 : losMenoresA 5 [2]
    3 : if 2 < 5 then 2 : losMenoresA 5 [] else losMenoresA 5 []
    3 : 2 : losMenoresA 5 []
    3 : 2 : []
-}


{- TRES PATRONES DE ALGORITMOS SOBRE LISTAS:

  - Reduce : Dada una lista, computar algún valor sobre ella. Por ejemplo, calcular
             la longitud, la suma de elementos, el elemento más grande ,etc.
  - Map:     Dada una lista, devolver una nueva lista donde los elementos han sufrido
             alguna transformacion. La longitud de la lista no cambia.
             Ejemplo mapSumaPar
  - Filter:  Dada una lista, devolver una nueva lista con los elementos que cumplen
             cierta condición. La longitud de la nueva lista puede ser menor a la original.
             Ejemplo losMenoresA
-}

-- Ejercicios para hacer la de práctica:
--  mapSumaPar, todoVerdad, algunaVerdad, losDistintosA, mapLongitudes, losDeLongitudMayorA
