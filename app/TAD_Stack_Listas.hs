module TAD_Stack_Listas (Stack, emptyST, isEmptyST, push, top, pop) where

-- Creo el tipo algebraico para representar la pila
data Stack a = Stack [a] 




-- Crea una pila vacía
emptyST :: Stack a
emptyST = Stack []


-- Dada una pila indica si está vacía.
isEmptyST :: Stack a -> Bool
isEmptyST (Stack [])     = True
isEmptyST (Stack (x:xs)) = False


-- Dados un elemento y una pila, agrega el elemento a la pila.
push :: a -> Stack a -> Stack a
push x (Stack xs) =  Stack (x:xs)


-- Dada un pila devuelve el elemento del tope de la pila.
top :: Stack a -> a
top (Stack (x:xs)) = x 
top (Stack []) = error "el stack esta vacio"


-- Dada una pila devuelve la pila sin el primer elemento.
pop :: Stack a -> Stack a
pop (Stack (x:xs)) = Stack xs
pop (Stack []) = error "el stack esta vacio"











---------- No dar importancia, es para que se muestren más lindo ------
instance Show a => Show (Stack a) where
    show (Stack xs) = "~"++ mostrarItems xs ++ "]"

mostrarItems :: Show a => [a] -> String
mostrarItems    []    = ""
mostrarItems    [x]   = show x
mostrarItems (x:y:ys) = show x ++ "," ++ mostrarItems (y:ys)
