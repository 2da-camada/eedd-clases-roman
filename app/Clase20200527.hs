module Clase20200527 where
import TAD_Map_ListaSinClavesRepetidas
{- Hoy creamos la implementación más sencilla del Map: usamos una lista de (clave,valor) donde
no se repiten las claves
-}

---------------------------------------- El tipo MAYBE -------------------------

-- data Maybe a = Nothing | Just a 

-- Se utiliza en situaciones en donde no siempre es posible obtener una respuesta válida
-- y evitar que en los casos que no haya respuesta válida se lance un error

-- Por ejemplo, si quisieramos hacer una versión "segura" de head (que devuelve el primer elemento de una lista)
-- podríamos codificarla de la siguiente forma:

-- Version normal: Notese que el caso de [] no está definido
headNormal :: [a] -> a
headNormal (x:xs) = x

-- Version segura: Si es lista vacía devuelve Nothing ("nada"). Sino, devuelve el primer elemento (Just x = "simplemente x")
headSegura :: [a] -> Maybe a
headSegura   []   = Nothing
headSegura (x:xs) = Just x


----------------------------------- El TAD Map (tambien llamado diccionario) -------------------------


-- Definimos algunos valores del tipo de datos abstracto Map

map1  = assocM 1 "uno" (assocM 3 "tres" emptyM)  --                              {3->"tres", 1->"uno"}
map1' = assocM 3 "CUATRO" map1                   -- piso el valor de la clave 3  {3->"CUATRO", 1->"uno"}


-- Ejercicio 1.1: Busca todas las claves dadas en la lista
buscar :: Eq k => [k] -> Map k v -> [Maybe v]
buscar   []   m = []
buscar (x:xs) m = lookupM m x : buscar xs m


-- Dado un Map, devuelve todos sus valores. Esta función no está en la práctica
valores :: Eq k => Map k v -> [v]
valores m = maybesToList (buscar (domM m) m)

maybesToList :: [Maybe a] -> [a]
maybesToList      []       = []
maybesToList (Nothing :xs) = maybesToList xs
maybesToList ((Just a):xs) = a : maybesToList xs


-- Indica si en el map se encuentran todas las claves dadas
estanTodas :: Eq k => [k] -> Map k v -> Bool
estanTodas xs m = estanTodas' xs (domM m)

estanTodas' :: Eq a => [a] -> [a] -> Bool
estanTodas'   []   ys = True
estanTodas' (x:xs) ys = (x `elem` ys) && estanTodas' xs ys


-- Agrega las asociaciones dadas al map
asociar :: Eq k => [(k, v)] -> Map k v -> Map k v
asociar      []    m = m 
asociar ((k,v):xs) m = asociar xs (assocM k v m)



-- Nota: se debe notar que esta recursión (similar al apilar2 de la clase de 20/05) funciona
-- correctamente. Se van agregando las asociaciones en el orden de la lista.

mapConEl1Pisado = asociar [(1,3), (2,4), (1,5)] emptyM  -- {1->5, 2->4}




