module Clase20200603 where
import TAD_Map_ListaSinClavesRepetidas
import TAD_MultiSet_Map


{- En esta clave hicimos un alto para recordar y comparar las estructuras de datos vistas hasta ahora.
   Luego hicimos algunos ejericios con Map de la practica 5 -}


--Dada una lista de elementos construye un Map que relaciona la posición que ocupa en la lista
--con el valor que está en dicha posicion 
indexar :: [a] -> Map Int a
indexar xs = indexar' xs emptyM 1

indexar' :: [a] -> Map Int a -> Int -> Map Int a
indexar' [] m _ = m
indexar' (x:xs) m n = assocM n x (indexar' xs m (n+1))   -- Acá está la parte intersante
                                                         -- el assocM se está usando como un (:) 


-- Dado un String, devuelve un map con las asociaciones de letras -> cantidad de veces que aparece
-- Esta implementación funciona bien porque se ejecuta primero lo que va quedando en parentesis,
-- de esta forma, los primeras letras del string pisan a las que están más adentro
ocurrencias :: String -> Map Char Int
ocurrencias []     = emptyM
ocurrencias (x:xs) = assocM x (apariciones x (x:xs)) (ocurrencias xs)

-- Funcion auxiliar. Es la misma de la practica 1, ejercicio 8
apariciones :: Eq a => a -> [a] -> Int
apariciones x [] = 0
apariciones x (y:ys) = if (x == y)
                           then 1 + apariciones x ys
                           else     apariciones x ys


--------------- Multiset -------------------------

mset1 = addMS 8 (addMS 3 (emptyMS))                    -- {{3,8}}
mset2 = addMS 3 (addMS 8 (addMS 7 (addMS 3 emptyMS)))  -- {3,7,3}


 